package com.apps.sikedip.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.apps.sikedip.MainActivity;
import com.apps.sikedip.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FirebaseNotificationService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseNotifService";

    Context context;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        context = this;
        // TODO(developer): Handle FCM messages here.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Data: " + remoteMessage.getData());
        Log.e(TAG, "Notification: " + remoteMessage.getNotification());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
            receiveNotif(remoteMessage.getData(), remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }

        // Check if message contains a notification payload.
        Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            receiveNotif(remoteMessage.getData(), remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }
    }



    private void receiveNotif(Map<String, String> data, String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        String title_notif = data.get("title");

        intent.putExtra("title", title_notif);
        switch (title_notif){
            case "panic":
                String lat = data.get("lat");
                String lng = data.get("lng");
                String no_hp = data.get("no_hp");
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("no_hp", no_hp);
                break;
            case "pengumuan":
                String isi = data.get("isi");
                String judul = data.get("judul");
                intent.putExtra("isi", isi);
                intent.putExtra("judul", judul);
                break;
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        buildNotif(pendingIntent, title, message);
    }

    private void buildNotif(PendingIntent pendingIntent, String title, String message) {
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.logo_mobile)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }
}
