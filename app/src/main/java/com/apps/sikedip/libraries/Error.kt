package com.apps.sikedip.libraries

import android.app.Activity
import android.content.Context
import android.support.v4.app.FragmentActivity

import com.android.volley.AuthFailureError
import com.android.volley.NetworkError
import com.android.volley.NoConnectionError
import com.android.volley.ParseError
import com.android.volley.ServerError
import com.android.volley.TimeoutError
import com.android.volley.VolleyError

class Error {
    lateinit var activity: FragmentActivity
    lateinit var context: Context

    constructor() {

    }

    constructor(FragmentActivity: Activity):this() {
        this.activity = activity
    }

    constructor(ctx: Context):this() {
        this.context = ctx
    }

    fun checkOnErrorVolleyNetwork(error: VolleyError, callback:ErrorCallback) {
        if (error is TimeoutError || error is NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
//            this.activity.container.snackbar("Jaringan Lemah Timeout")
            callback.errorContaint(error)

        } else if (error is AuthFailureError) {
            //Error indicating that there was an Authentication Failure while performing the request
//            this.activity.container.snackbar("Request koneksi failed")
            callback.errorContaint(error)
        } else if (error is ServerError) {
            //Indicates that the server responded with a error response
//            this.activity.container.snackbar("Halaman Tidak Ditemukan")
            callback.errorContaint(error)
        } else if (error is NetworkError) {
            //Indicates that there was network error while performing the request
//            this.activity.container.snackbar("Failed Request pada saat Koneksi")
            callback.errorContaint(error)
        } else if (error is ParseError) {
            // Indicates that the server response could not be parsed
//            this.activity.container.snackbar("Failed Request tidak ada respon")
            callback.errorContaint(error)
        } else {
//            this.activity.container.snackbar("Failed")
            callback.errorContaint(error)
        }
    }

    fun checkOnErrorVolleyNetworkWithMessage(error: VolleyError):String {
        if (error is TimeoutError || error is NoConnectionError) {
            return "Jaringan Lemah Timeout"
        } else if (error is AuthFailureError) {
            return "Request koneksi failed"
        } else if (error is ServerError) {
            return "Halaman Tidak Ditemukan"
        } else if (error is NetworkError) {
            return "Failed Request pada saat Koneksi"
        } else if (error is ParseError) {
            return "Failed Request tidak ada respon"
        } else {
            return "Failed"
        }
    }

    fun checkOnErrorVolleyNetworkCustom(error: VolleyError) {
        if (error is TimeoutError || error is NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            Message(this.activity).showMessage("Tidak ada koneksi atau koneksi Timeout ")

        } else if (error is AuthFailureError) {
            //Error indicating that there was an Authentication Failure while performing the request
            Message(this.activity).showMessage("Request koneksi failed")

        } else if (error is ServerError) {
            //Indicates that the server responded with a error response
            Message(this.activity).showMessage("Halaman Tidak Ditemukan")

        } else if (error is NetworkError) {
            //Indicates that there was network error while performing the request
            Message(this.activity).showMessage("Failed Request pada saat Koneksi")

        } else if (error is ParseError) {
            // Indicates that the server response could not be parsed
            Message(this.activity).showMessage("Failed Request tidak ada respon")

        } else {
            Message(this.activity).showMessage("Failed")
        }
    }

    fun checkOnErrorVolleyNetwork(error: VolleyError, ctx: Context) {
        if (error is TimeoutError || error is NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            Message(ctx).showMessage("Tidak ada koneksi atau koneksi Timeout ")

        } else if (error is AuthFailureError) {
            //Error indicating that there was an Authentication Failure while performing the request
            Message(ctx).showMessage("Request koneksi failed")

        } else if (error is ServerError) {
            //Indicates that the server responded with a error response
            Message(ctx).showMessage("Halaman Tidak Ditemukan")

        } else if (error is NetworkError) {
            //Indicates that there was network error while performing the request
            Message(ctx).showMessage("Failed Request pada saat Koneksi")

        } else if (error is ParseError) {
            // Indicates that the server response could not be parsed
            Message(ctx).showMessage("Failed Request tidak ada respon")

        } else {
            Message(ctx).showMessage("Failed")
        }
    }


}
