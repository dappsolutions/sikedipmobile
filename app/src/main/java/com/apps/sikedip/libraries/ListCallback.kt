package com.apps.sikedip.libraries

import android.view.View

interface ListCallback {
  fun onExecuteViewHolder(view:View, position:Int)
}