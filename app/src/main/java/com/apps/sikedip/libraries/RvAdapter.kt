package com.apps.sikedip.libraries

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class RvAdapter(val list: ArrayList<Any?>, val callback: ListCallback, val activity: FragmentActivity, val layout: Int)
  : RecyclerView.Adapter<RvAdapter.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    var rowView = LayoutInflater.from(parent.context).inflate(layout, parent, false) as View
    return RvAdapter.ViewHolder(rowView)
  }

  override fun getItemCount(): Int {
    return list.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindItemView(callback, position)
  }

  class ViewHolder(val view:View):RecyclerView.ViewHolder(view){
    fun bindItemView(callback: ListCallback, position: Int) {
      callback.onExecuteViewHolder(view, position)
    }
  }

}