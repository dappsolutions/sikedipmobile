package com.apps.sikedip.libraries;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

//  PDFView document;
  FragmentActivity activity;

  RecyclerView rvView;
  RecyclerView.Adapter adapterList;

  Spinner spinView;
//  DtAdapter spinAdapter;

  ViewPager viewPager;
  TabLayout tabLayout;

  NavigationView navigationView;

  public Helper() {

  }

  public Helper(FragmentActivity act) {
    this.activity = act;
  }

  public Helper(FragmentActivity act, RecyclerView rv_view, RecyclerView.Adapter adapterList) {
    this.activity = act;
    this.rvView = rv_view;
    this.adapterList = adapterList;
  }

  public Helper(FragmentActivity act, Spinner sp_view) {
    this.activity = act;
    this.spinView = sp_view;
  }

  public Helper(FragmentActivity act, ViewPager viewPager, TabLayout tabLayout) {
    this.activity = act;
    this.viewPager = viewPager;
    this.tabLayout = tabLayout;
  }

  public Helper(NavigationView navView){
    this.navigationView = navView;
  }

  //format Y-m-d
  public String getDateToString(DatePicker datePicker) {
    String year = Integer.toString(datePicker.getYear());
    String month = Integer.toString(datePicker.getMonth());
    String day = Integer.toString(datePicker.getDayOfMonth());
    if (datePicker.getMonth() < 10) {
      month = "0" + month;
    }

    if (datePicker.getDayOfMonth() < 10) {
      day = "0" + day;
    }

    String date = year + "-" + month + "-" + day;
    return date;
  }

  public String getCurrentDateTime() {
    String waktu = "";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    waktu = dateFormat.format(date);
    return waktu;
  }

  public String getCurrentDate() {
    String waktu = "";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    waktu = dateFormat.format(date);
    return waktu;
  }

  public void setRecycleview(){
    LinearLayoutManager layoutManager = new LinearLayoutManager(this.activity);
    this.rvView.setLayoutManager(layoutManager);
    this.rvView.setHasFixedSize(true);
    this.rvView.setAdapter(adapterList);
    adapterList.notifyDataSetChanged();
  }

  public void setRecycleviewHorizontal(){
    LinearLayoutManager layoutManager = new LinearLayoutManager(this.activity, LinearLayoutManager.HORIZONTAL, false);
    this.rvView.setLayoutManager(layoutManager);
    this.rvView.setHasFixedSize(true);
    this.rvView.setAdapter(adapterList);
    adapterList.notifyDataSetChanged();
  }

  public void setRecycleviewGrid(){
    GridLayoutManager manager = new GridLayoutManager(this.activity, 2,
            GridLayoutManager.VERTICAL, false);
    rvView.setLayoutManager(manager);
    rvView.setHasFixedSize(true);
    rvView.setAdapter(adapterList);
  }
  public void setRecycleviewGrid(int column){
    GridLayoutManager manager = new GridLayoutManager(this.activity, column,
            GridLayoutManager.VERTICAL, false);
    rvView.setLayoutManager(manager);
    rvView.setHasFixedSize(true);
    rvView.setAdapter(adapterList);
  }

  public void setDatePickerDialog(final EditText edt){
    final Calendar c = Calendar.getInstance();
    int mYear = c.get(Calendar.YEAR);
    int mMonth = c.get(Calendar.MONTH);
    int mDay = c.get(Calendar.DAY_OF_MONTH);

    DatePickerDialog datePickerDialog = new DatePickerDialog(this.activity,
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                edt.setText(year + "-" + (month + 1) + "-" + day);
              }
            }, mYear, mMonth, mDay);
    datePickerDialog.show();
  }

  public void setTimePicker(final EditText edt){
    final Calendar c = Calendar.getInstance();
    int mHour = c.get(Calendar.HOUR_OF_DAY);
    int mMinute = c.get(Calendar.MINUTE);

    // Launch Time Picker Dialog
    TimePickerDialog timePickerDialog = new TimePickerDialog(this.activity,
            new TimePickerDialog.OnTimeSetListener() {

              @Override
              public void onTimeSet(TimePicker view, int hourOfDay,
                                    int minute) {

                edt.setText(hourOfDay + ":" + minute);
              }
            }, mHour, mMinute, false);
    timePickerDialog.show();
  }

  public JSONArray getJsonArrayData(String response, String result){
    JSONArray jsonArray = null;
    try {
      JSONObject jsonObject;
      jsonObject = new JSONObject(response);
      jsonArray = jsonObject.getJSONArray(result);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return jsonArray;
  }

  public void setAdapterViewPager(FragmentPagerAdapter adapterViewPager){
    this.viewPager.setAdapter(adapterViewPager);
    this.tabLayout.setupWithViewPager(this.viewPager);
  }

  public void setNavigationDrawer(final NavCallback callback){
    this.navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        callback.getItemData(item);
        return true;
      }
    });
  }

  public void shareLink(String message){
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_SEND);
    intent.setType("text/plain");
    intent.putExtra(Intent.EXTRA_SUBJECT, "");
    intent.putExtra(Intent.EXTRA_TEXT, message);
    this.activity.startActivity(Intent.createChooser(intent, "Bagikan ke"));
  }
}
