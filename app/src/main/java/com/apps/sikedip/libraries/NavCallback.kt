package com.apps.sikedip.libraries

import android.view.MenuItem

interface NavCallback{
    fun getItemData(item:MenuItem)
}