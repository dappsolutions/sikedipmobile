package com.apps.sikedip.libraries;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

public class Session {
    FragmentActivity activity;
    SharedPreferences setSession;
    Context context;

    public Session(FragmentActivity activity) {
        this.activity = activity;
        setSession = this.activity.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public Session(Context ctx) {
        this.context = ctx;
        setSession = this.context.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public void setUserId(String playerName) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("user_id", playerName);
        data_ses.apply();
    }

    public void setNama(String nama) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("nama", nama);
        data_ses.apply();
    }

    public void setFoto(String foto) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("foto", foto);
        data_ses.apply();
    }

    public void setTaruna(String taruna) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("taruna", taruna);
        data_ses.apply();
    }

    public void setNoHpTaruna(String no_hp) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("no_hp", no_hp);
        data_ses.apply();
    }

    public void setHakAkses(String hak_akses) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("hak_akses", hak_akses);
        data_ses.apply();
    }

    public void setBaseUrl(String url) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("url", url);
        data_ses.apply();
    }

    public void setKompi(String kompi) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("kompi", kompi);
        data_ses.apply();
    }

    public void setTarunaNamaWali(String taruna_nama_wali) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("taruna_nama_wali", taruna_nama_wali);
        data_ses.apply();
    }

    public String getUserId() {
        return setSession.getString("user_id", "missing");
    }

    public String getNama() {
        return setSession.getString("nama", "missing");
    }

    public String getFoto() {
        return setSession.getString("foto", "missing");
    }

    public String getTaruna() {
        return setSession.getString("taruna", "missing");
    }

    public String getNoHpTaruna() {
        return setSession.getString("no_hp", "missing");
    }

    public String getHakAkses() {
        return setSession.getString("hak_akses", "missing");
    }

    public String getBaseUrl() {
        return setSession.getString("url", "missing");
    }

    public String getKompi() {
        return setSession.getString("kompi", "missing");
    }

    public String getTarunaNamaWali() {
        return setSession.getString("taruna_nama_wali", "missing");
    }

    public void clearSession() {
        setSession.edit().remove("user_id").commit();
        setSession.edit().remove("taruna").commit();
        setSession.edit().remove("nama").commit();
        setSession.edit().remove("foto").commit();
        setSession.edit().remove("no_hp").commit();
        setSession.edit().remove("hak_akses").commit();
        setSession.edit().remove("url").commit();
        setSession.edit().remove("kompi").commit();
        setSession.edit().remove("taruna_nama_wali").commit();
    }

}
