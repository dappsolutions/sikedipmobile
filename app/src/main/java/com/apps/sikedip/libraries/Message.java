package com.apps.sikedip.libraries;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

public class Message {
  Context context;
  ProgressDialog pDialog;

  public Message(Context ctx) {
    this.context = ctx;
    pDialog = new ProgressDialog(ctx);
  }

  public void showMessage(String message) {
    Toast.makeText(this.context, message, Toast.LENGTH_SHORT).show();
  }

  public void showMessage(String message, Boolean is_long) {
    if (is_long) {
      Toast.makeText(this.context, message, Toast.LENGTH_LONG).show();
    } else {
      Toast.makeText(this.context, message, Toast.LENGTH_SHORT).show();
    }
  }

  public void showDialog(String title, String msg) {
    final AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);
//        dialog.setIcon(R.drawable.ic_logout);
    String judul = "Informasi";
    if (!title.isEmpty()) {
      judul = title;
    }
    dialog.setTitle(judul);
    dialog.setMessage(msg);
    dialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int i) {
        dialog.cancel();
      }
    });
    dialog.show();
  }

  public void showDialog(String title, String msg, final DialogCallback callback) {
    final AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);
//        dialog.setIcon(R.drawable.ic_logout);
    String judul = "Informasi";
    if (!title.isEmpty()) {
      judul = title;
    }
    dialog.setTitle(judul);
    dialog.setMessage(msg);
    dialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int i) {
        dialog.cancel();
        callback.okAction(dialog, i);
      }
    });
    dialog.show();
  }

  public void showLoading(String message){
    pDialog.setMessage(message);
    pDialog.show();
  }

  public void showLoading(String title, String message){
    pDialog.setTitle(title);
    pDialog.setMessage(message);
    pDialog.show();
  }

  public void hideLoading(){
    if(pDialog.isShowing()){
      pDialog.hide();
    }
  }
}
