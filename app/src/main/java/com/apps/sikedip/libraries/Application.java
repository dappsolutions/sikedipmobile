package com.apps.sikedip.libraries;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import com.apps.sikedip.MainActivity;


public class Application {
  FragmentActivity activity;

  public Application(){

  }

  public Application(FragmentActivity act) {
    this.activity = act;
  }

  public void moveFragment(FragmentActivity activity, int layout, Fragment fragment){
    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager()
            .beginTransaction().replace(layout, fragment);
    fragmentTransaction.addToBackStack(null);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public void moveSingleFragment(FragmentActivity activity, int layout, Fragment fragment){
    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager()
            .beginTransaction().replace(layout, fragment);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public void firstActivity(Context ctx){
    Intent intent = new Intent(ctx, MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    ctx.startActivity(intent);
  }
}
