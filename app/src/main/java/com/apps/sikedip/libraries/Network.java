package com.apps.sikedip.libraries;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Network {
    public static String BASE_URL = "";
    Activity activity;
    DownloadManager downloadManager;
    Context context;

    public Network(Context ctx) {
        this.context = ctx;
    }

    public Network(Activity activity) {
        this.activity = activity;
    }

    public void downloadFile(String url_file) {
        downloadManager = (DownloadManager) this.context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url_file);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION);
        Long reference = downloadManager.enqueue(request);
    }

    public void volleyRequestWithErrorCustom(String url, final VolleyCallbackWithError callback) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

        };
        Volley.newRequestQueue(context).add(request);
    }

    public void volleyRequestWithErrorCustom(String url, final Map<String, String> postData, final VolleyCallbackWithError callback) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return postData;
            }

        };
        Volley.newRequestQueue(context).add(request);
    }

}
