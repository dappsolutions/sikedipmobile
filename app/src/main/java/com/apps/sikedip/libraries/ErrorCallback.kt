package com.apps.sikedip.libraries

import com.android.volley.VolleyError

interface ErrorCallback{
    fun errorContaint(error: VolleyError)
}