package com.apps.sikedip.libraries

import android.content.DialogInterface

interface DialogCallback{
    fun okAction(dialog: DialogInterface, i: Int)
}