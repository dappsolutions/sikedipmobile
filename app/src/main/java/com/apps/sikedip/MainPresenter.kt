package com.apps.sikedip

import android.content.Intent
import android.util.Log
import com.apps.sikedip.libraries.Application
import com.apps.sikedip.libraries.Session
import com.apps.sikedip.modules.login.view.Login
import com.apps.sikedip.modules.lokasi_panic.view.LokasiPanic
import com.apps.sikedip.modules.mainpage.view.MainPageActivity
import org.jetbrains.anko.startActivity

class MainPresenter(val main: MainActivity) {
    init {
        checkingNotification()
    }

    private fun initPage() {
        val user = Session(main).userId
        Log.e("User", user)
        if (!user.equals("missing") && !user.equals("")) {
            main.startActivity<MainPageActivity>()
        } else {
            Application().moveSingleFragment(main, R.id.frameContent, Login())
        }
    }

    private fun checkingNotification() {
        if (main.intent.extras != null) {
            Log.e("InitHal", "Notif")
            val title = main.intent.getStringExtra("title")

            when (title) {
                "panic" -> {
                    val lat = main.intent.getStringExtra("lat")
                    val lng = main.intent.getStringExtra("lng")
                    val no_hp = main.intent.getStringExtra("no_hp")
                    if (lat != null) {
                        val intent = Intent(main, MainPageActivity::class.java)
                        intent.putExtra("lat", lat)
                        intent.putExtra("lng", lng)
                        intent.putExtra("no_hp", no_hp)
                        intent.putExtra("title", title)
                        main.startActivity(intent)
                    } else {
                        initPage()
                    }
                }
                "pengumuman" -> {
                    val judul = main.intent.getStringExtra("judul")
                    val isi = main.intent.getStringExtra("isi")
                    if (judul != null) {
                        val intent = Intent(main, MainPageActivity::class.java)
                        intent.putExtra("judul", judul)
                        intent.putExtra("isi", isi)
                        intent.putExtra("title", title)
                        main.startActivity(intent)
                    } else {
                        initPage()
                    }
                }
            }
        } else {
            Log.e("InitHal", "No Notif")
            initPage()
        }
    }
}