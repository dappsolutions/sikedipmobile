package com.apps.sikedip.modules.kesalahan.model

import com.google.gson.annotations.SerializedName

class KesalahanModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("jenis")
    var jenis:String,
    @SerializedName("point")
    var point:String,
    @SerializedName("tanggal")
    var tanggal:String,
    @SerializedName("semester")
    var semester:String,
    @SerializedName("keterangan")
    var keterangan:String,
    @SerializedName("total_poin")
    var total_poin:String,
    @SerializedName("prodi")
    var prodi:String
)

class KesalahanModelResponse(var data:List<KesalahanModel>)