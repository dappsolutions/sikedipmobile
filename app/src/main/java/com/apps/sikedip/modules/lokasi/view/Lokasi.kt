package com.apps.sikedip.modules.lokasi.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.modules.lokasi.presenter.LokasiPresenter
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener
import com.mapbox.mapboxsdk.location.OnLocationClickListener
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.apps.sikedip.R
import kotlinx.android.synthetic.main.lokasi_view.*
import android.support.annotation.NonNull
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import android.graphics.Color
import com.apps.sikedip.libraries.Message


class Lokasi : Fragment(), OnMapReadyCallback, OnLocationClickListener, PermissionsListener,
    OnCameraTrackingChangedListener {


    lateinit var presenter:LokasiPresenter
    lateinit var mapboxMap:MapboxMap
    lateinit var permissionsManager:PermissionsManager
    lateinit var locationComponent:LocationComponent
    private var isInTrackingMode: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Mapbox.getInstance(context!!, "sk.eyJ1IjoiZG9kaWtpdG4iLCJhIjoiY2p1bzZhbG84MjFqdDQ5bjcyeGhkMXZqdSJ9.JXLSQXodPwCjSPjUtqxCkg")
        val view = LayoutInflater.from(context).inflate(R.layout.lokasi_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = LokasiPresenter(this)
        mapLokasi.onCreate(savedInstanceState)
        mapLokasi.getMapAsync(this)
    }

    override fun onMapReady(mapBox: MapboxMap) {
        mapboxMap = mapBox
        mapboxMap.setStyle(Style.MAPBOX_STREETS, object : Style.OnStyleLoaded {
            override fun onStyleLoaded(@NonNull style: Style) {
                enableLocationComponent(style)
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(context)) {

            // Create and customize the LocationComponent's options
            val customLocationComponentOptions = LocationComponentOptions.builder(context!!)
                .elevation(5f)
                .accuracyAlpha(.6f)
                .accuracyColor(Color.RED)
                .build()

            // Get an instance of the component
            locationComponent = mapboxMap.locationComponent

            val locationComponentActivationOptions = LocationComponentActivationOptions.builder(context!!, loadedMapStyle)
                .locationComponentOptions(customLocationComponentOptions)
                .build()

            // Activate with options
            locationComponent.activateLocationComponent(locationComponentActivationOptions)

            // Enable to make component visible
            locationComponent.isLocationComponentEnabled = true

            // Set the component's camera mode
            locationComponent.cameraMode = CameraMode.TRACKING

            // Set the component's render mode
            locationComponent.renderMode = RenderMode.COMPASS

            // Add the location icon click listener
            locationComponent.addOnLocationClickListener(this)

            // Add the camera tracking listener. Fires if the map camera is manually moved.
            locationComponent.addOnCameraTrackingChangedListener(this)

            //click to activate
            if (!isInTrackingMode) {
                isInTrackingMode = true
                locationComponent.cameraMode = CameraMode.TRACKING
                locationComponent.zoomWhileTracking(16.0)
                Message(context).showMessage("Tracking Not Enabled")
            } else {
                val lat = locationComponent.lastKnownLocation!!.latitude
                val lng = locationComponent.lastKnownLocation!!.longitude
                Message(context).showMessage("Tracking Enabled")
//                mapLokasi.getMapAsync(this)
            }

        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(activity)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onLocationComponentClick() {
        if(locationComponent.lastKnownLocation != null){
            val lat = locationComponent.lastKnownLocation!!.latitude
            val lng = locationComponent.lastKnownLocation!!.longitude
            Message(context).showMessage("Lokasi Sekarang $lat dan $lng")
        }
    }

    override fun onCameraTrackingDismissed() {
        isInTrackingMode = false
    }

    override fun onCameraTrackingChanged(currentMode: Int) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        Message(context).showMessage("Explantion Permission")
    }

    override fun onPermissionResult(granted: Boolean) {
        if(granted){
            mapboxMap.setStyle(Style.MAPBOX_STREETS, object : Style.OnStyleLoaded {
                override fun onStyleLoaded(@NonNull style: Style) {
                    enableLocationComponent(style)
                }
            })
        }else{
            Message(context).showMessage("Permission Denied")
        }
    }

    override fun onStart() {
        super.onStart()
        mapLokasi.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapLokasi.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapLokasi.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapLokasi.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapLokasi.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapLokasi.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapLokasi.onLowMemory()
    }
}