package com.apps.sikedip.modules.chat_admin.presenter

import android.util.Log
import android.view.Gravity.RIGHT
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.chat_admin.model.ChatAdminModel
import com.apps.sikedip.modules.chat_admin.model.ChatAdminModelResponse
import com.apps.sikedip.modules.chat_admin.view.ChatAdmin
import com.google.gson.Gson
import kotlinx.android.synthetic.main.chat_admin_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class ChatAdminPresenter(val main: ChatAdmin) {
    lateinit var adapter: RvAdapter
    var hakAkses = Session(main.context).hakAkses
    val baseUrl = Session(main.context).baseUrl
    init {
        dataChatToAdmin()
    }

    fun dataChatToAdmin() {
        val url = ApiService.URL.baseUrl(baseUrl,"chat_admin", "getDataChat")
        val params = HashMap<String, String>()

        if (main.arguments != null) {
            val userParam = main.arguments!!.get("user").toString()
            Log.e("userparam", userParam)
            if (!userParam.equals("")) {
                params["user"] = userParam
                Log.e("userparam", "OK")
            } else {
                params["user"] = Session(main.context).userId
                Log.e("userparam", "Not OK")
            }
        } else {
            params["user"] = Session(main.context).userId
        }

        params["table"] = "chat_admin"
        if (hakAkses.equals("Manajemen")) {
            params["table"] = "chat_manajemen"
        }
        if (hakAkses.equals("Admin")) {
            params["table"] = "chat_admin"
        }

        params["kompi"] = Session(main.context).kompi
        main.loading.visibility = View.VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DataChat", result)
                    main.loading.visibility = View.GONE
                    val data = Gson().fromJson(result, ChatAdminModelResponse::class.java)
                    setListDataChat(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = View.GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListDataChat(data: List<ChatAdminModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                val contentChat = view.findViewById(R.id.contentChat) as LinearLayout
                val txtPesan = view.findViewById(R.id.txtChat) as TextView
                val txtFrom = view.findViewById(R.id.txtFrom) as TextView
                val txtTanggal = view.findViewById(R.id.txtDateMessage) as TextView

                txtPesan.text = data[position].message
                txtTanggal.text = data[position].tgl_kirim

                if (data[position].hak_akses.equals(Session(main.context).hakAkses)) {
                    contentChat.gravity = RIGHT
                    if (Session(main.context).hakAkses.equals("Admin") || Session(main.context).hakAkses.equals("Manajemen")) {
                        txtFrom.text = Session(main.context).hakAkses
                    }
                } else {
                    if (Session(main.context).hakAkses.equals("Admin") || Session(main.context).hakAkses.equals("Manajemen")) {
                        if(!Session(main.context).tarunaNamaWali.equals("null")){
                            txtFrom.text = data[position].from_label+" ("+Session(main.context).tarunaNamaWali+")"
                        }else{
                            txtFrom.text = data[position].from_label
                        }
                    } else {
                        txtFrom.text = "Admin " + data[position].from_label
                    }
                }


            }
        }, main.activity!!, R.layout.list_chat)
        Helper(main.activity, main.rvListChat, adapter).setRecycleview()
    }

    fun sendMessage() {
        val params = HashMap<String, String>()
        params["message"] = main.edtMessage.text.toString()
        params["from"] = Session(main.context).userId
        params["from_label"] = Session(main.context).nama
        params["hak_akses"] = Session(main.context).hakAkses
        params["kompi"] = Session(main.context).kompi
        if (main.arguments != null) {
            params["to"] = main.arguments!!.get("user").toString()
            Log.e("PAramTo", main.arguments!!.get("user").toString())
        }

        params["table"] = "chat_admin"
        if (hakAkses.equals("Manajemen")) {
            params["table"] = "chat_manajemen"
        }
        if (hakAkses.equals("Admin")) {
            params["table"] = "chat_admin"
        }

        val url = ApiService.URL.baseUrl(baseUrl,"chat_admin", "sendMessage")

        val dialog = Message(main.context)


        if(!Session(main.context).hakAkses.equals("Manajemen")){
            if(Session(main.context).kompi.equals("0")){
                Message(main.context).showMessage("Anda Belum Terdaftar pada Kompi")
                return
            }
        }

        dialog.showMessage("Proses Mengirim Pesan...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Kirim", result)
                    dialog.hideLoading()
                    main.edtMessage.setText("")
                    if (JSONObject(result).get("is_valid").toString().equals("true")) {
                        Message(main.context).showMessage("Pesan Sukses Dikirim")
                        dataChatToAdmin()
                    } else {
                        Message(main.context).showMessage("Gagal Mengirim Pesan")
                    }
                }

                override fun onError(error: VolleyError) {
                    dialog.hideLoading()
                    main.edtMessage.setText("")
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }
}