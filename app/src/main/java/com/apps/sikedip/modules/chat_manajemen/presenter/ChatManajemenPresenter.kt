package com.apps.sikedip.modules.chat_manajemen.presenter

import android.util.Log
import android.view.Gravity.RIGHT
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.chat_manajemen.ChatManajemenModel
import com.apps.sikedip.modules.chat_manajemen.ChatManajemenModelResponse
import com.apps.sikedip.modules.chat_manajemen.view.ChatManajemen
import com.google.gson.Gson
import kotlinx.android.synthetic.main.chat_manajemen_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class ChatManajemenPresenter(val main: ChatManajemen) {
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataChatToAdmin()
    }

    fun dataChatToAdmin() {
        val url = ApiService.URL.baseUrl(baseUrl,"chat_admin", "getDataChatManajemen")
        val params = HashMap<String, String>()
        params["user"] = Session(main.context).userId

        main.loading.visibility = View.VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DataChatMan", result)
                    main.loading.visibility = View.GONE
                    val data = Gson().fromJson(result, ChatManajemenModelResponse::class.java)
                    setListDataChat(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = View.GONE
                    val msg = com.apps.sikedip.libraries.Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListDataChat(data: List<ChatManajemenModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                val contentChat = view.findViewById(R.id.contentChat) as LinearLayout
                val txtPesan = view.findViewById(R.id.txtChat) as TextView
                val txtFrom = view.findViewById(R.id.txtFrom) as TextView
                val txtTanggal = view.findViewById(R.id.txtDateMessage) as TextView

                txtPesan.text = data[position].message
                txtTanggal.text = data[position].tgl_kirim

                if(data[position].hak_akses.equals(Session(main.context).hakAkses)){
                    contentChat.gravity = RIGHT
                }else{
                    txtFrom.text = "Manajemen "+data[position].from_label
                }
            }
        }, main.activity!!, R.layout.list_chat_manajemen)
        Helper(main.activity, main.rvListChat, adapter).setRecycleview()
    }

    fun sendMessage() {
        val params = HashMap<String, String>()
        params["message"] = main.edtMessage.text.toString()
        params["from"] = Session(main.context).userId
        params["from_label"] = Session(main.context).nama
        params["hak_akses"] = Session(main.context).hakAkses
        if (main.arguments != null) {
            params["to"] = main.arguments!!.get("user").toString()
        }
        val url = ApiService.URL.baseUrl(baseUrl,"chat_admin", "sendMessageManajemen")

        val dialog = Message(main.context)
        dialog.showMessage("Proses Mengirim Pesan...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Kirim", result)
                    dialog.hideLoading()
                    main.edtMessage.setText("")
                    if (JSONObject(result).get("is_valid").toString().equals("true")) {
                        Message(main.context).showMessage("Pesan Sukses Dikirim")
                        dataChatToAdmin()
                    } else {
                        Message(main.context).showMessage("Gagal Mengirim Pesan")
                    }
                }

                override fun onError(error: VolleyError) {
                    dialog.hideLoading()
                    main.edtMessage.setText("")
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }
}