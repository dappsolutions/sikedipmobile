package com.apps.sikedip.modules.mainpage.presenter

import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.dashboard.view.Dashboard
import com.apps.sikedip.modules.mainpage.view.MainPageActivity
import com.apps.sikedip.modules.menu_chat_admin.view.MenuChatAdmin
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main_page.*
import org.jetbrains.anko.doAsync

class MainPagePresenter(val main: MainPageActivity) {
    val baseUrl = Session(main).baseUrl
    init {
        initPage()
    }

    private fun initPage() {
        val user = Session(main).userId
        val hak_akses = Session(main).hakAkses
        if (!user.equals("missing") && !user.equals("")) {
            //goto beranda
            if(hak_akses.equals("Admin")
                || hak_akses.equals("Superadmin")
                || hak_akses.equals("Manajemen")){
                Application().moveSingleFragment(main, R.id.frameContent, MenuChatAdmin())
            }else{
                Application().moveSingleFragment(main, R.id.frameContent, Dashboard())
            }
        }
    }

    fun setDataUserLogin() {
        var foto = Session(main).foto
        var nama = Session(main).nama
        val headerView = main.nav_bar.getHeaderView(0)
        if(Session(main).hakAkses.equals("Taruna")){
            if (!foto.equals("missing") && !foto.equals("") && !foto.equals("null")) {
                val imgProfil = headerView.findViewById(R.id.imgProfil) as CircleImageView
                Picasso.with(main).load(foto).into(imgProfil)
            }
        }
        val txtNama = headerView.findViewById(R.id.txtUsername) as TextView
        txtNama.text = nama
    }

    fun setTokenUser() {
        val Url = ApiService.URL.baseUrl(baseUrl,"token", "setToken")
        val params = HashMap<String, String>()
        params["token"] = FirebaseInstanceId.getInstance().getToken().toString()
        params["user"] = Session(main).userId

        doAsync {
            Network(main.applicationContext).volleyRequestWithErrorCustom(Url, params, object : VolleyCallbackWithError{
                override fun onSuccess(result: String) {
                    Message(main).showMessage("Data Berhasil Diperbaharui")
                }

                override fun onError(error: VolleyError) {
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main).showMessage(msg)
                }
            })
        }
    }
}