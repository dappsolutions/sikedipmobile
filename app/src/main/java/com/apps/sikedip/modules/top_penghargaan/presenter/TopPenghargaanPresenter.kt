package com.apps.sikedip.modules.top_penghargaan.presenter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.penghargaan.model.PenghargaanModel
import com.apps.sikedip.modules.penghargaan.model.PenghargaanModelResponse
import com.apps.sikedip.modules.top_penghargaan.view.TopPenghargaan
import com.google.gson.Gson
import kotlinx.android.synthetic.main.top_penghargaan_view.*
import org.jetbrains.anko.doAsync

class TopPenghargaanPresenter(val main:TopPenghargaan){
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataPenghargaanTaruna()
    }

    fun dataPenghargaanTaruna() {
        val url = ApiService.URL.baseUrl(baseUrl,"penghargaan", "getDataPenghargaanTopTen")
        val params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna
        params["user"] = Session(main.context).userId

        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, PenghargaanModelResponse::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<PenghargaanModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtJenis = view.findViewById(R.id.txtJenis) as TextView
                val txtPoint = view.findViewById(R.id.txtPoint) as TextView
                val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                val txtSemester = view.findViewById(R.id.txtSemester) as TextView
                val txtKet = view.findViewById(R.id.txtKet) as TextView

                txtNama.text = data[position].nama_taruna
                txtJenis.text = data[position].prodi
                txtPoint.text = "("+data[position].skor+")"
                txtTanggal.text = "Semester "+data[position].semester
                txtKet.text = data[position].keterangan

                txtSemester.text = ""
                txtSemester.visibility = GONE
            }
        }, main.activity!!, R.layout.list_penghargaan)
        Helper(main.activity, main.rvListPenghargaan, adapter).setRecycleview()
    }
}