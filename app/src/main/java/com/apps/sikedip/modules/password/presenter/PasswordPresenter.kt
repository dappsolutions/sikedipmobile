package com.apps.sikedip.modules.password.presenter

import android.text.TextUtils
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.dashboard.view.Dashboard
import com.apps.sikedip.modules.password.view.Password
import kotlinx.android.synthetic.main.change_password_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class PasswordPresenter(val main: Password){
    val baseUrl = Session(main.context).baseUrl
    init {

    }

    fun changePassword(){

        if(TextUtils.isEmpty(main.edtPasswordbaru.text.toString())){
            main.edtPasswordbaru.setError("Harus Diisi")
            return
        }

        if(TextUtils.isEmpty(main.edtPasswordlama.text.toString())){
            main.edtPasswordlama.setError("Harus Diisi")
            return
        }

        var url = ApiService.URL.baseUrl(baseUrl,"password", "changePassword")
        val params = HashMap<String, String>()
        params["user"] = Session(main.context).userId
        params["password_lama"] = main.edtPasswordlama.text.toString()
        params["password_baru"] = main.edtPasswordbaru.text.toString()

        var msg = Message(main.context)
        msg.showLoading("Proses Ganti Password...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError{
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    if(JSONObject(result).get("is_valid").toString().equals("true")){
                        msg.showMessage("Password Berhasil dirubah")
                        Application().moveFragment(main.activity, R.id.frameContent, Dashboard())
                    }else{
                        msg.showMessage(JSONObject(result).get("message").toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msgError = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(msgError)
                }
            })
        }
    }
}