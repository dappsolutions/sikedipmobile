package com.apps.sikedip.modules.menu_chat_admin.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.libraries.Application
import com.apps.sikedip.modules.chat_taruna.view.ChatTaruna
import com.apps.sikedip.modules.chat_wali.view.ChatWali
import com.apps.sikedip.modules.menu_chat_admin.presenter.MenuChatAdminPresenter
import kotlinx.android.synthetic.main.menu_chat_admin_view.*

class MenuChatAdmin : Fragment(){
    lateinit var presenter:MenuChatAdminPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.menu_chat_admin_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = MenuChatAdminPresenter(this)

        cardTaruna.setOnClickListener {
            Application().moveFragment(activity, R.id.frameContent, ChatTaruna())
        }

        cardWaliMurid.setOnClickListener {
            Application().moveFragment(activity, R.id.frameContent, ChatWali())
        }
    }
}