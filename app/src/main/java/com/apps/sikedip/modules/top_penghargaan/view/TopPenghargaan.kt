package com.apps.sikedip.modules.top_penghargaan.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.top_penghargaan.presenter.TopPenghargaanPresenter

class TopPenghargaan : Fragment(){
    lateinit var presenter:TopPenghargaanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.top_penghargaan_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = TopPenghargaanPresenter(this)
    }
}