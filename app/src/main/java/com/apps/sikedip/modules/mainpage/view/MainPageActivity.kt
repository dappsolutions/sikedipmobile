package com.apps.sikedip.modules.mainpage.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.apps.sikedip.MainActivity
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.asrama.view.Asrama
import com.apps.sikedip.modules.dashboard.view.Dashboard
import com.apps.sikedip.modules.izin.view.Izin
import com.apps.sikedip.modules.jadwal_rutin.view.JadwalRutin
import com.apps.sikedip.modules.kesalahan.view.Kesalahan
import com.apps.sikedip.modules.lokasi.view.LokasiMap
import com.apps.sikedip.modules.lokasi_panic.view.LokasiPanic
import com.apps.sikedip.modules.mainpage.presenter.MainPagePresenter
import com.apps.sikedip.modules.menu_chat.view.MenuChat
import com.apps.sikedip.modules.menu_chat_admin.view.MenuChatAdmin
import com.apps.sikedip.modules.password.view.Password
import com.apps.sikedip.modules.penghargaan.view.Penghargaan
import com.apps.sikedip.modules.pengumuman.views.Pengumuman
import com.apps.sikedip.modules.psikologi.views.Psikologi
import com.apps.sikedip.modules.sakit.view.SakitView
import com.apps.sikedip.modules.top_kesalahan.view.TopKesalahan
import com.apps.sikedip.modules.top_penghargaan.view.TopPenghargaan
import kotlinx.android.synthetic.main.activity_main_page.*
import org.jetbrains.anko.startActivity

class MainPageActivity : AppCompatActivity() {

    lateinit var presenter : MainPagePresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_page)
        leftContent()

        presenter = MainPagePresenter(this)
        presenter.setTokenUser()
        presenter.setDataUserLogin()
        checkingNotification()
    }

    private fun leftContent() {
        val hak_akses = Session(this).hakAkses
        when(hak_akses){
            "Manajemen" -> {
                nav_bar.menu.findItem(R.id.nav_dashboard).isVisible = false
                nav_bar.menu.findItem(R.id.nav_kesalahan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_jadwal).isVisible = false
                nav_bar.menu.findItem(R.id.nav_izin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_sakit).isVisible = false
                nav_bar.menu.findItem(R.id.nav_penghargaan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_chat_admin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_pengumuman).isVisible = false
            }
            "Admin" -> {
                nav_bar.menu.findItem(R.id.nav_dashboard).isVisible = false
                nav_bar.menu.findItem(R.id.nav_kesalahan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_jadwal).isVisible = false
                nav_bar.menu.findItem(R.id.nav_izin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_sakit).isVisible = false
                nav_bar.menu.findItem(R.id.nav_penghargaan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_chat_admin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_pengumuman).isVisible = false
            }
            "Superadmin" -> {
                nav_bar.menu.findItem(R.id.nav_dashboard).isVisible = false
                nav_bar.menu.findItem(R.id.nav_kesalahan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_jadwal).isVisible = false
                nav_bar.menu.findItem(R.id.nav_izin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_sakit).isVisible = false
                nav_bar.menu.findItem(R.id.nav_penghargaan).isVisible = false
                nav_bar.menu.findItem(R.id.nav_chat_admin).isVisible = false
                nav_bar.menu.findItem(R.id.nav_pengumuman).isVisible = false
            }
            "Taruna" -> {
                nav_bar.menu.findItem(R.id.nav_chat_taruna).isVisible = false
            }
            "Wali Murid" -> {
                nav_bar.menu.findItem(R.id.nav_chat_taruna).isVisible = false
                nav_bar.menu.findItem(R.id.nav_lokasi).isVisible = false
            }
        }

        Helper(nav_bar).setNavigationDrawer(object : NavCallback{
            override fun getItemData(item: MenuItem) {
                drawer_layout.closeDrawers()
                when(item.itemId){
                    R.id.nav_dashboard -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Dashboard())
                    }
                    R.id.nav_kesalahan -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Kesalahan())
                    }
                    R.id.nav_topkesalahan -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, TopKesalahan())
                    }
                    R.id.nav_penghargaan -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Penghargaan())
                    }
                    R.id.nav_toppenghargaan -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, TopPenghargaan())
                    }
                    R.id.nav_izin -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Izin())
                    }
                    R.id.nav_jadwal -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, JadwalRutin())
                    }
                    R.id.nav_sakit -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, SakitView())
                    }
                    R.id.nav_psikologi -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Psikologi())
                    }
                    R.id.nav_asrama -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Asrama())
                    }
                    R.id.nav_lokasi ->{
                        startActivity<LokasiMap>()
                    }
                    R.id.nav_chat_admin -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, MenuChat())
                    }
                    R.id.nav_chat_taruna -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, MenuChatAdmin())
                    }
                    R.id.nav_password -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Password())
                    }
                    R.id.nav_pengumuman -> {
                        Application().moveFragment(this@MainPageActivity, R.id.frameContent, Pengumuman())
                    }
                    R.id.nav_sign_out ->{
                        Session(this@MainPageActivity).clearSession()
                        this@MainPageActivity.finish()
                        val login = Intent(this@MainPageActivity, MainActivity::class.java)
                        login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(login)
                    }
                }
            }
        })
    }

    private fun checkingNotification() {
        if (intent.extras != null) {
            val title = intent.getStringExtra("title")
            when(title){
                "panic" -> {
                    val lat = intent.getStringExtra("lat")
                    val lng = intent.getStringExtra("lng")
                    val no_hp = intent.getStringExtra("no_hp")
                    if (lat != null) {
                        val intent = Intent(this, LokasiMap::class.java)
                        intent.putExtra("lat", lat)
                        intent.putExtra("lng", lng)
                        intent.putExtra("no_hp", no_hp)
                        startActivity(intent)
                    }
                }
                "pengumuman" -> {
                    Application().moveFragment(this@MainPageActivity, R.id.frameContent, Pengumuman())
                }
            }
        }
    }
}
