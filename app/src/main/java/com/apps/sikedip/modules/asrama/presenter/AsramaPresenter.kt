package com.apps.sikedip.modules.asrama.presenter

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.asrama.model.AsramaModel
import com.apps.sikedip.modules.asrama.model.AsramaModelResponse
import com.apps.sikedip.modules.asrama.view.Asrama
import com.google.gson.Gson
import kotlinx.android.synthetic.main.asrama_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class AsramaPresenter(val main: Asrama){
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataIzinTaruna()
    }

    fun dataIzinTaruna() {
        val url = ApiService.URL.baseUrl(baseUrl,"asrama", "getDataAsramaTaruna")
        val params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna
        params["userId"] = Session(main.context).userId

        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DataAsrama", result)
                    main.loading.visibility = GONE
                    try{
                        main.txtAsrama.text = "ASRAMA : "+JSONObject(result).get("asrama").toString()
                        main.txtKamar.text = "KAMAR : "+JSONObject(result).get("kamar_taruna").toString()
                        val data = Gson().fromJson(result, AsramaModelResponse::class.java)
                        setListData(data.data)
                    }catch (ex:Exception){
                        Log.e("ErrorASrama", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<AsramaModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtProdi = view.findViewById(R.id.txtProdi) as TextView
                val txtSemester = view.findViewById(R.id.txtSemester) as TextView

                txtNama.text = data[position].nama_taruna
                txtProdi.text = data[position].prodi
                txtSemester.text = "Semester "+data[position].semester
            }
        }, main.activity!!, R.layout.list_asrama)
        Helper(main.activity, main.rvListAsrama, adapter).setRecycleview()
    }
}