package com.apps.sikedip.modules.asrama.model

import com.google.gson.annotations.SerializedName

data class AsramaModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("kamar_taruna")
    var kamar_taruna:String,
    @SerializedName("prodi")
    var prodi:String,
    @SerializedName("semester")
    var semester:String
)

data class AsramaModelResponse(var data:List<AsramaModel>)