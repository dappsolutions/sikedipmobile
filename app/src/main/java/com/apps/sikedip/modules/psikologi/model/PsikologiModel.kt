package com.apps.sikedip.modules.psikologi.model

import com.google.gson.annotations.SerializedName

data class PsikologiModel (
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("jam")
    var jam:String,
    @SerializedName("tanggal")
    var tanggal:String,
    @SerializedName("keterangan")
    var keterangan:String
)

data class PsikologiModelResponse(var data:List<PsikologiModel>)