package com.apps.sikedip.modules.password.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.password.presenter.PasswordPresenter
import kotlinx.android.synthetic.main.change_password_view.*

class Password : Fragment(){
    lateinit var presenter:PasswordPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.change_password_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = PasswordPresenter(this)

        cardSimpan.setOnClickListener {
            presenter.changePassword()
        }
    }
}