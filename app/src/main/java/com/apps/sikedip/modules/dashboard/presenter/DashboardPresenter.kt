package com.apps.sikedip.modules.dashboard.presenter

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.chat_admin.view.ChatAdmin
import com.apps.sikedip.modules.chat_manajemen.view.ChatManajemen
import com.apps.sikedip.modules.dashboard.model.KeluarModel
import com.apps.sikedip.modules.dashboard.model.KeluarModelResponse
import com.apps.sikedip.modules.dashboard.model.Sakit
import com.apps.sikedip.modules.dashboard.model.SakitResponses
import com.apps.sikedip.modules.dashboard.view.Dashboard
import com.apps.sikedip.modules.psikologi.model.PsikologiModel
import com.apps.sikedip.modules.psikologi.model.PsikologiModelResponse
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dashboard_view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class DashboardPresenter(val main: Dashboard) {
    lateinit var adapter: RvAdapter
    val msg = Message(main.context)
    val baseUrl = Session(main.context).baseUrl
    init {
        refresh()
    }

    fun refresh() {
        getNamaTarunaWali()
        getDataKesalahan()
        getDataPenghargaan()
        getTopTarunaSakit()
        getTopTarunaIzinKeluar()
        getTopTarunaPsikologi()
        setDataUserLogin()
    }

    fun getNamaTarunaWali() {
        if(Session(main.context).hakAkses.equals("Wali Murid")){
            main.txtNamaWaliTaruna.text = "Wali dari : "+Session(main.context).tarunaNamaWali
            main.txtNamaWaliTaruna.visibility = VISIBLE
        }
    }

    fun getTopTarunaPsikologi() {
        var url = ApiService.URL.baseUrl(baseUrl,"psikologi", "getDataPsikologiTaruna")
        var params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna

        Log.e("taruna", Session(main.context).taruna)

        msg.showLoading("Proses Updating Data Taruna Izin Keluar...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Psikologi", result)
                    msg.hideLoading()
                    val data = Gson().fromJson(result, PsikologiModelResponse::class.java)
                    if(data.data.isEmpty()){
                        main.cardPsikologi.visibility = VISIBLE
                    }else{
                        setListTarunaPsikologi(data.data)
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListTarunaPsikologi(data: List<PsikologiModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtJam = view.findViewById(R.id.txtJam) as TextView
                val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                val txtKeterangan = view.findViewById(R.id.txtKeterangan) as TextView

                txtNama.text = data[position].nama_taruna
                txtTanggal.text = data[position].tanggal
                txtJam.text = data[position].jam
                txtKeterangan.text = data[position].keterangan
            }
        }, main.activity!!, R.layout.list_psikologi_view)
        Helper(main.activity, main.rvListPsikologi, adapter).setRecycleview()
    }

    fun setDataUserLogin() {
        var foto = Session(main.context).foto
        if (!foto.equals("missing") && !foto.equals("") && !foto.equals("null")) {
            Picasso.with(main.context).load(foto).into(main.imgTaruna)
        }
    }

    fun getTopTarunaIzinKeluar() {
        var url = ApiService.URL.baseUrl(baseUrl,"izin", "getIzinTarunaData")
        var params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna

        Log.e("taruna", Session(main.context).taruna)

        msg.showLoading("Proses Updating Data Taruna Izin Keluar...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Izin Keluar", result)
                    msg.hideLoading()
                    val data = Gson().fromJson(result, KeluarModelResponse::class.java)
                    if(data.data.isEmpty()){
                        main.cardKeluar.visibility = VISIBLE
                    }else{
                        setListTarunaIzinKeluar(data.data)
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListTarunaIzinKeluar(data: List<KeluarModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                var txtNama = view.findViewById(R.id.txtNama) as TextView
                var txtJenis = view.findViewById(R.id.txtJenis) as TextView
                var txtSemester = view.findViewById(R.id.txtSemester) as TextView
                var txtJam = view.findViewById(R.id.txtJam) as TextView
                var txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                var txtKeterangan = view.findViewById(R.id.txtKeterangan) as TextView
                var cardIzin = view.findViewById(R.id.contentIzin) as LinearLayout
                cardIzin.setBackgroundColor(Color.parseColor("#6495ED"))

                txtNama.text = ""
                txtNama.setTextColor(Color.parseColor("#ffffff"))
                txtNama.visibility = GONE
                txtJenis.text = data[position].jenis
                txtJenis.setTextColor(Color.parseColor("#ffffff"))
                txtSemester.text = "Semester " + data[position].semester
                txtSemester.setTextColor(Color.parseColor("#ffffff"))
                txtJam.text = data[position].jam
                txtJam.setTextColor(Color.parseColor("#ffffff"))
                txtTanggal.text = data[position].tanggal_awal + " - " + data[position].tanggal_akhir
                txtTanggal.setTextColor(Color.parseColor("#ffffff"))
                txtKeterangan.text = data[position].keterangan
                txtKeterangan.setTextColor(Color.parseColor("#ffffff"))
            }
        }, main.activity!!, R.layout.list_izin)
        Helper(main.activity, main.rvListKeluar, adapter).setRecycleview()
    }

    private fun getTopTarunaSakit() {
        var url = ApiService.URL.baseUrl(baseUrl,"sakit", "getDataIzinTaruna")
        var params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna

        Log.e("taruna", Session(main.context).taruna)

        msg.showLoading("Proses Updating Data Taruna Sakit...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Sakit", result)
                    msg.hideLoading()
                    val data = Gson().fromJson(result, SakitResponses::class.java)
                    if(data.data.isEmpty()){
                        main.cardSakit.visibility = VISIBLE
                    }else{
                        setListTarunaSakit(data.data)
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListTarunaSakit(data: List<Sakit>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
            override fun onExecuteViewHolder(view: View, position: Int) {
                var txtNama = view.findViewById(R.id.txtNama) as TextView
                var txtJam = view.findViewById(R.id.txtJam) as TextView
                var txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                var txtKeterangan = view.findViewById(R.id.txtKeterangan) as TextView
                var txtTitle = view.findViewById(R.id.txtTitle) as TextView
                var cardTaruna = view.findViewById(R.id.cardTaruna) as CardView

                cardTaruna.setCardBackgroundColor(Color.parseColor("#D2691E"))

                txtNama.text = ""
                txtNama.setTextColor(Color.parseColor("#ffffff"))
                txtNama.visibility = GONE
                txtJam.text = data[position].jam
                txtJam.setTextColor(Color.parseColor("#ffffff"))
                txtTanggal.text = data[position].tanggal
                txtTanggal.setTextColor(Color.parseColor("#ffffff"))
                txtKeterangan.text = data[position].keterangan
                txtKeterangan.setTextColor(Color.parseColor("#ffffff"))
                txtTitle.setTextColor(Color.parseColor("#ffffff"))
            }
        }, main.activity!!, R.layout.list_taruna_sakit)
        Helper(main.activity, main.rvListSakit, adapter).setRecycleview()
    }

    fun getDataPenghargaan() {
        val url = ApiService.URL.baseUrl(baseUrl,"penghargaan", "getDataPenghargaanTaruna")
        var params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna

        msg.showLoading("Proses Updating Data Penghargaan...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    try {
                        msg.hideLoading()
                        val jumlah = JSONObject(result).get("jumlah").toString()
                        val status = JSONObject(result).get("status").toString()
                        main.txtPenghargaan.text = "Poin Penghargaan = $jumlah ($status)"
                        if (jumlah.toInt() >= 80) {
                            main.imgPenghargaan.visibility = View.VISIBLE
                        }
                    } catch (ex: Exception) {
                        Log.e("getPenghargaan", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun getDataKesalahan() {
        val url = ApiService.URL.baseUrl(baseUrl,"kesalahan", "getDataKesalahanTaruna")
        var params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna
        params["user"] = Session(main.context).userId


        msg.showLoading("Proses Updating data Kesalahan...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    try {
                        msg.hideLoading()
                        Log.e("DataKesalahan", result)
                        val jumlah = JSONObject(result).get("jumlah").toString()
                        val status = JSONObject(result).get("status").toString()
                        main.txtKesalahan.text = "Poin Kesalahan = $jumlah ($status)"
                        if (jumlah.toInt() >= 50) {
                            main.imgKesalahan.visibility = View.VISIBLE
                        }
                    } catch (ex: Exception) {
                        Log.e("getKesalahan", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun panicBtnActive(lat: Double, lng: Double) {
        val url = ApiService.URL.baseUrl(baseUrl,"dashboard", "panicBtnActive")
        val params = HashMap<String, String>()
        params["lat"] = lat.toString()
        params["lng"] = lng.toString()
        params["user"] = Session(main.context).userId
        params["taruna"] = Session(main.context).nama
        params["no_hp"] = Session(main.context).noHpTaruna
        params["kompi"] = Session(main.context).kompi

        val dialog = Message(main.context)
        dialog.showLoading("Proses Mengirim Sinyal Bahaya ke Semua Perangkat Kompi")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Panic", result)
                    dialog.hideLoading()
                    Message(main.context).showMessage("Sukses Mengirim Tanda Ke Semua Perangkat Kompi")
                }

                override fun onError(error: VolleyError) {
                    dialog.hideLoading()
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    @SuppressLint("RestrictedApi")
    fun setPanicButton() {
        val hak_akses = Session(main.context).hakAkses
        when (hak_akses) {
            "Wali Murid" -> {
                main.panicBtn.visibility = GONE
                main.txtPanicBtn.visibility = GONE
            }
            "Admin" -> {
                main.panicBtn.visibility = GONE
                main.txtPanicBtn.visibility = GONE
            }
            "Superadmin" -> {
                main.panicBtn.visibility = GONE
                main.txtPanicBtn.visibility = GONE
            }
            "Manajemen" -> {
                main.panicBtn.visibility = GONE
                main.txtPanicBtn.visibility = GONE
            }
        }
    }

    fun chatToAdmin() {
        val dialog = AlertDialog.Builder(main.activity!!)
        var judul = "Pesan"
        dialog.setTitle(judul)
        dialog.setMessage("Apa anda yakin chat dengan Admin ?")
        dialog.setNeutralButton("Ya") { dialog, i ->
            Application().moveFragment(main.activity, R.id.frameContent, ChatAdmin())
        }
        dialog.setNegativeButton("Tidak", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog?.dismiss()
            }
        })
        dialog.setOnCancelListener {
            it.cancel()
        }
        dialog.show()
    }

    fun chatToManajemen() {
        val dialog = AlertDialog.Builder(main.activity!!)
        var judul = "Pesan"
        dialog.setTitle(judul)
        dialog.setMessage("Apa anda yakin chat dengan Manajemen ?")
        dialog.setNeutralButton("Ya") { dialog, i ->
            Application().moveFragment(main.activity, R.id.frameContent, ChatManajemen())
        }
        dialog.setNegativeButton("Tidak", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog?.dismiss()
            }
        })
        dialog.setOnCancelListener {
            it.cancel()
        }
        dialog.show()
    }
}