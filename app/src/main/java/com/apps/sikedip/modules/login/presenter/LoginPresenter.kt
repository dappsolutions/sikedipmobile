package com.apps.sikedip.modules.login.presenter

import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.util.Log
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.chat_manajemen.view.ChatManajemen
import com.apps.sikedip.modules.login.view.Login
import com.apps.sikedip.modules.mainpage.view.MainPageActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.login_view.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.json.JSONObject

class LoginPresenter(val main : Login){
    init {

    }


    fun signIn(){
        var user = main.edtUsername.text.toString()
        var password = main.edtPassword.text.toString()

        if(TextUtils.isEmpty(user)){
            main.edtUsername.setError("Username Harus Diisi")
            return
        }

        if(TextUtils.isEmpty(password)){
            main.edtPassword.setError("Password Harus Diisi")
            return
        }

        val dialog = AlertDialog.Builder(main.activity!!)
        var judul = "Jaringan"
        dialog.setTitle(judul)
        dialog.setMessage("Pilih Jaringan yang Digunakan")
        dialog.setNeutralButton("Lokal") { dialog, i ->
            Session(main.context).baseUrl = "http://192.168.3.10/sikedip/mobile"
            exeLogin()
        }
        dialog.setNegativeButton("Internet", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                Session(main.context).baseUrl = "http://103.94.6.34:4321/sikedip/mobile"
                exeLogin()
            }
        })
        dialog.setOnCancelListener {
            it.cancel()
        }
        dialog.show()
    }

    fun exeLogin(){
        var user = main.edtUsername.text.toString()
        var password = main.edtPassword.text.toString()
        var baseUrl = Session(main.context).baseUrl

        var url = ApiService.URL.baseUrl(baseUrl,"login", "signIn")
        val params = HashMap<String, String>()
        params["username"] = user
        params["password"] = password

        val msg = Message(main.context)
        msg.showLoading("Proses Sign In...")
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object: VolleyCallbackWithError{
                override fun onSuccess(result: String) {
                    Log.e("Login", result)
                    msg.hideLoading()
                    if(JSONObject(result).get("is_valid").toString().equals("true")){
                        Session(main.context).userId = JSONObject(result).get("user_id").toString()
                        Session(main.context).foto = JSONObject(result).get("foto").toString()
                        Session(main.context).nama = JSONObject(result).get("nama").toString()
                        Session(main.context).taruna = JSONObject(result).get("taruna").toString()
                        Session(main.context).hakAkses = JSONObject(result).get("hak_akses").toString()
                        Session(main.context).noHpTaruna = JSONObject(result).get("no_hp").toString()
                        Session(main.context).kompi = JSONObject(result).get("kompi").toString()
                        Session(main.context).tarunaNamaWali = JSONObject(result).get("taruna_nama_wali").toString()
                        main.context!!.startActivity<MainPageActivity>()
                    }else{
                        msg.showMessage("Login Gagal Dilakukan")
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val msgError = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    main.activity!!.container.snackbar(msgError)
                }
            })
        }
    }
}