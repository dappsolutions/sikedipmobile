package com.apps.sikedip.modules.chat_wali.presenter

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.chat_admin.view.ChatAdmin
import com.apps.sikedip.modules.chat_wali.model.ChatWaliModel
import com.apps.sikedip.modules.chat_wali.model.ChatWaliModelResponses
import com.apps.sikedip.modules.chat_wali.view.ChatWali
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.chat_wali_view.*
import org.jetbrains.anko.doAsync

class ChatWaliPresenter(val main:ChatWali){
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataIzinWali()
    }

    fun dataIzinWali() {
        val url = ApiService.URL.baseUrl(baseUrl,"chat_admin", "getListChatMasukWali")
        val params = HashMap<String, String>()
        val hak_akses = Session(main.context).hakAkses
        if(hak_akses.equals("Superadmin") || hak_akses.equals("Admin")){
            params["table"] = "chat_admin"
            params["to"] = "1"
        }
        if(hak_akses.equals("Manajemen")){
            params["table"] = "chat_manajemen"
            params["to"] = "5"
        }
        params["hak_akses"] = "wali"
        params["kompi"] = Session(main.context).kompi
        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, ChatWaliModelResponses::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<ChatWaliModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtChat = view.findViewById(R.id.txtChat) as TextView
                val txtDateMessage = view.findViewById(R.id.txtDateMessage) as TextView
                val imgProfil = view.findViewById(R.id.imgProfil) as CircleImageView
                val contentChat = view.findViewById(R.id.contentChat) as LinearLayout

                if(!data[position].foto.equals("") && !data[position].foto.equals("null")){
                    Picasso.with(main.context).load(data[position].foto).into(imgProfil)
                }
                txtNama.text = data[position].username
                txtChat.text = data[position].message
                txtDateMessage.text = data[position].createddate

                contentChat.setOnClickListener {
                    val chatWali = ChatAdmin()
                    val bundle = Bundle()
                    bundle.putString("user", data[position].from)
                    chatWali.arguments = bundle
                    Application().moveFragment(main.activity, R.id.frameContent, chatWali)
                }
            }
        }, main.activity!!, R.layout.list_data_chat)
        Helper(main.activity, main.rvListIChat, adapter).setRecycleview()
    }
}