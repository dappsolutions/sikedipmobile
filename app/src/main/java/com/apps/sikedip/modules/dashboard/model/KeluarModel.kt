package com.apps.sikedip.modules.dashboard.model

import com.google.gson.annotations.SerializedName

data class KeluarModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("tanggal_awal")
    var tanggal_awal:String,
    @SerializedName("tanggal_akhir")
    var tanggal_akhir:String,
    @SerializedName("jam")
    var jam:String,
    @SerializedName("jenis")
    var jenis:String,
    @SerializedName("keterangan")
    var keterangan:String,
    @SerializedName("semester")
    var semester:String
)

data class KeluarModelResponse(var data:List<KeluarModel>)