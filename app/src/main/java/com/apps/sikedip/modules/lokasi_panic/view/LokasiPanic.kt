package com.apps.sikedip.modules.lokasi_panic.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.lokasi_panic.presenter.LokasiPanicPresenter
import com.mapbox.mapboxsdk.Mapbox

class LokasiPanic : Fragment(){
    lateinit var presenter:LokasiPanicPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Mapbox.getInstance(context!!, "sk.eyJ1IjoiZG9kaWtpdG4iLCJhIjoiY2p1bzZhbG84MjFqdDQ5bjcyeGhkMXZqdSJ9.JXLSQXodPwCjSPjUtqxCkg")
        val view = LayoutInflater.from(context).inflate(R.layout.lokasi_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = LokasiPanicPresenter(this)

        val lat = arguments!!.getString("lat").toDouble()
        val lng = arguments!!.getString("lng").toDouble()
        presenter.initMapCurrentLokasi(lat, lng)
    }
}