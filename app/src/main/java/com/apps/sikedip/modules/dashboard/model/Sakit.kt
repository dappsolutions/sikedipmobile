package com.apps.sikedip.modules.dashboard.model

import com.google.gson.annotations.SerializedName

data class Sakit(
    @SerializedName("id")
    var id:String,
    @SerializedName("no_taruna")
    var no_taruna:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("tanggal")
    var tanggal:String,
    @SerializedName("jam")
    var jam:String,
    @SerializedName("keterangan")
    var keterangan:String
)

data class SakitResponses(var data:List<Sakit>)