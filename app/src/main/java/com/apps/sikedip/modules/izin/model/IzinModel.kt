package com.apps.sikedip.modules.izin.model

import com.google.gson.annotations.SerializedName

class IzinModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("jam")
    var jam:String,
    @SerializedName("tanggal")
    var tanggal:String,
    @SerializedName("semester")
    var semester:String,
    @SerializedName("jenis")
    var jenis:String,
    @SerializedName("keterangan")
    var keterangan:String
)

class IzinModelResponse(var data:List<IzinModel>)