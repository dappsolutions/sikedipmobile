package com.apps.sikedip.modules.penghargaan.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.penghargaan.presenter.PenghargaanPresenter

class Penghargaan : Fragment(){
    lateinit var presenter:PenghargaanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.penghargaan_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = PenghargaanPresenter(this)
    }
}