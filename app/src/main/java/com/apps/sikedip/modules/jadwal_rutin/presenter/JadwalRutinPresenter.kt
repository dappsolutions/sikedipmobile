package com.apps.sikedip.modules.jadwal_rutin.presenter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.jadwal_rutin.model.JadwalRutinModel
import com.apps.sikedip.modules.jadwal_rutin.model.JadwalRutinModelResponse
import com.apps.sikedip.modules.jadwal_rutin.view.JadwalRutin
import com.google.gson.Gson
import kotlinx.android.synthetic.main.jadwal_rutin_view.*
import org.jetbrains.anko.doAsync

class JadwalRutinPresenter(val main:JadwalRutin){
    lateinit var adapter:RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataJadwalTaruna()
    }

    fun dataJadwalTaruna() {
        val url = ApiService.URL.baseUrl(baseUrl,"rutin", "getDataRutin")
        val params = HashMap<String, String>()
        params["user"] = Session(main.context).userId

        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, JadwalRutinModelResponse::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<JadwalRutinModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtHari = view.findViewById(R.id.txtHari) as TextView
                val txtPukul = view.findViewById(R.id.txtPukul) as TextView
                val txtKegiatan = view.findViewById(R.id.txtKegiatan) as TextView

                txtHari.text = data[position].hari
                txtPukul.text = data[position].pukul
                txtKegiatan.text = data[position].kegiatan
            }
        }, main.activity!!, R.layout.list_jadwal)
        Helper(main.activity, main.rvListJadwal, adapter).setRecycleview()
    }
}