package com.apps.sikedip.modules.pengumuman.model

import com.google.gson.annotations.SerializedName

data class PengumumanModel (
    @SerializedName("id")
    var id:String,
    @SerializedName("judul")
    var judul:String,
    @SerializedName("isi")
    var isi:String,
    @SerializedName("ditujukan")
    var ditujukan:String
)

data class PengumumanModelResponse(val data:List<PengumumanModel>)