package com.apps.sikedip.modules.dashboard.view

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.view.Gravity.LEFT
import com.apps.sikedip.R
import com.apps.sikedip.libraries.Message
import com.apps.sikedip.modules.dashboard.presenter.DashboardPresenter
import com.google.firebase.iid.FirebaseInstanceId
import com.mapbox.mapboxsdk.Mapbox
import kotlinx.android.synthetic.main.dashboard_view.*


class Dashboard : Fragment() {

    lateinit var presenter: DashboardPresenter

    private var gps: GPSTracker? = null
    var lat: Double = 0.0
    var long: Double = 0.0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.dashboard_view, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val acti = activity as AppCompatActivity
        acti.setSupportActionBar(toolbar)
        setHasOptionsMenu(true)
        presenter = DashboardPresenter(this)
        Log.e("TokenFM", FirebaseInstanceId.getInstance().getToken().toString())

        setCurrentLokasi()
        //panic button
        presenter.setPanicButton()

        panicBtn.setOnClickListener {
            presenter.panicBtnActive(lat, long)
        }

        chatAdminBtn.setOnClickListener {
            presenter.chatToAdmin()
        }

        chatManajemenBtn.setOnClickListener {
            presenter.chatToManajemen()
        }

        toolbar.setNavigationOnClickListener {
            val drawer_layout = activity!!.findViewById(R.id.drawer_layout) as DrawerLayout
            drawer_layout.openDrawer(LEFT)
        }
    }

    fun setCurrentLokasi() {
        try {
            if (ContextCompat.checkSelfPermission(
                    Mapbox.getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        gps = GPSTracker(context)
        if (gps!!.canGetLocation()) {
            lat = gps!!.getLatitude()
            long = gps!!.getLongitude()

            Log.e("lat", lat.toString())
            Log.e("lng", long.toString())
        } else {
            gps!!.showSettingsAlert()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.nav_menu_dashboard, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId

        if(id == R.id.ic_refresh){
            Message(context).showMessage("Refresh")
            presenter.refresh()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}