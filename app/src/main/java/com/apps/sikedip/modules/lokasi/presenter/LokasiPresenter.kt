package com.apps.sikedip.modules.lokasi.presenter

import android.annotation.SuppressLint
import android.support.annotation.NonNull
import com.apps.sikedip.libraries.Message
import com.apps.sikedip.modules.lokasi.view.Lokasi
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.lokasi_view.*
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.maps.MapboxMap





class LokasiPresenter(val main:Lokasi){
    init {

    }

    @SuppressLint("MissingPermission")
    fun initMapCurrentLokasi(lat: Double, lng: Double) {
        main.mapLokasi.getMapAsync {


            it.setStyle(Style.MAPBOX_STREETS){
//                style -> enableLocationComponent()
            }
            it.addMarker(MarkerOptions()
                .position(LatLng(lat, lng)
                    ).title("Lokasi Saya"))

            it.setOnMarkerClickListener(object : MapboxMap.OnMarkerClickListener {
                override fun onMarkerClick(@NonNull marker: Marker): Boolean {

                    // Show a toast with the title of the selected marker
                    Message(main.context).showMessage(marker.title)
                    return true
                }
            })
        }
    }
}