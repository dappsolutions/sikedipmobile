package com.apps.sikedip.modules.penghargaan.model

import com.google.gson.annotations.SerializedName

class PenghargaanModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("nama_taruna")
    var nama_taruna:String,
    @SerializedName("prestasi")
    var prestasi:String,
    @SerializedName("tanggal")
    var tanggal:String,
    @SerializedName("semester")
    var semester:String,
    @SerializedName("skor")
    var skor:String,
    @SerializedName("keterangan")
    var keterangan:String,
    @SerializedName("total_poin")
    var total_poin:String,
    @SerializedName("prodi")
    var prodi:String
)

class PenghargaanModelResponse(var data:List<PenghargaanModel>)