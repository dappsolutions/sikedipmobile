package com.apps.sikedip.modules.lokasi.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View.VISIBLE
import com.apps.sikedip.R
import com.apps.sikedip.libraries.Message

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mapbox.mapboxsdk.Mapbox
import kotlinx.android.synthetic.main.activity_lokasi_map.*

class LokasiMap : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private var gps: GPSTracker? = null
    var lat: Double = 0.0
    var long: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lokasi_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        setCurrentLokasi()
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        if (intent.extras != null) {
            val latt = intent.getStringExtra("lat").toDouble()
            val longt = intent.getStringExtra("lng").toDouble()
            val no_hp = intent.getStringExtra("no_hp")
            Message(this).showMessage(no_hp)
            if (latt != null) {
                lat = latt
                long = longt

                txtNoTelp.visibility = VISIBLE
                txtNoTelp.text = " "+no_hp

                txtNoTelp.setOnClickListener {
                    Log.e("Telp Taruna", no_hp)
                    val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:"+no_hp))
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                        Log.e("Permissionelp", "Tidak Diijinkan")
                        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), 1)
                        return@setOnClickListener
                    }
                    this.startActivity(callIntent)
                }
            }
            Log.e("Lokasi", intent.getStringExtra("lat"))
        }
        val lokasiSaya = LatLng(lat, long)
        mMap.addMarker(MarkerOptions().position(lokasiSaya).title("Lokasi"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lokasiSaya))
    }

    fun setCurrentLokasi() {
        try {
            if (ContextCompat.checkSelfPermission(
                    Mapbox.getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        gps = GPSTracker(this)
        if (gps!!.canGetLocation()) {
            lat = gps!!.getLatitude()
            long = gps!!.getLongitude()
        } else {
            gps!!.showSettingsAlert()
        }
    }
}
