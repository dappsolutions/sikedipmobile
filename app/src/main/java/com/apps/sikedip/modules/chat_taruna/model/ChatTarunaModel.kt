package com.apps.sikedip.modules.chat_taruna.model

import com.google.gson.annotations.SerializedName

class ChatTarunaModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("username")
    var username:String,
    @SerializedName("from")
    var from:String,
    @SerializedName("foto")
    var foto:String,
    @SerializedName("message")
    var message:String,
    @SerializedName("createddate")
    var createddate:String
)

class ChatTarunaModelResponses(var data:List<ChatTarunaModel>)