package com.apps.sikedip.modules.chat_admin.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.chat_admin.presenter.ChatAdminPresenter
import kotlinx.android.synthetic.main.chat_admin_view.*

class ChatAdmin : Fragment(){
    lateinit var presenter: ChatAdminPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.chat_admin_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = ChatAdminPresenter(this)

        btnSend.setOnClickListener {
            presenter.sendMessage()
        }
    }
}