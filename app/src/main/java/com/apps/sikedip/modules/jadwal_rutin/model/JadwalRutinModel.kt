package com.apps.sikedip.modules.jadwal_rutin.model

import com.google.gson.annotations.SerializedName

class JadwalRutinModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("hari")
    var hari:String,
    @SerializedName("pukul")
    var pukul:String,
    @SerializedName("kegiatan")
    var kegiatan:String
)

class JadwalRutinModelResponse(var data:List<JadwalRutinModel>)