package com.apps.sikedip.modules.jadwal_rutin.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.jadwal_rutin.presenter.JadwalRutinPresenter

class JadwalRutin : Fragment(){
    lateinit var presenter:JadwalRutinPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.jadwal_rutin_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = JadwalRutinPresenter(this)
    }
}