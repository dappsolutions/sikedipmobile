package com.apps.sikedip.modules.login.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.login.presenter.LoginPresenter
import kotlinx.android.synthetic.main.login_view.*

class Login : Fragment(){
    lateinit var presenter:LoginPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.login_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = LoginPresenter(this)

        cardLogin.setOnClickListener {
            presenter.signIn()
        }
    }
}