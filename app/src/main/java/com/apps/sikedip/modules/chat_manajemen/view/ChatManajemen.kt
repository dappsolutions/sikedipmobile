package com.apps.sikedip.modules.chat_manajemen.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.chat_manajemen.presenter.ChatManajemenPresenter
import kotlinx.android.synthetic.main.chat_manajemen_view.*

class ChatManajemen : Fragment(){
    lateinit var presenter:ChatManajemenPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.chat_manajemen_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ChatManajemenPresenter(this)

        btnSend.setOnClickListener {
            presenter.sendMessage()
        }

    }
}