package com.apps.sikedip.modules.menu_chat.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.libraries.Application
import com.apps.sikedip.modules.chat_admin.view.ChatAdmin
import com.apps.sikedip.modules.chat_manajemen.view.ChatManajemen
import com.apps.sikedip.modules.menu_chat.presenter.MenuChatPresenter
import kotlinx.android.synthetic.main.menu_chat_view.*

class MenuChat : Fragment(){
    lateinit var presenter:MenuChatPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.menu_chat_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = MenuChatPresenter(this)

        cardAdmin.setOnClickListener {
            Application().moveFragment(activity, R.id.frameContent, ChatAdmin())
        }

        cardManajemen.setOnClickListener {
            Application().moveFragment(activity, R.id.frameContent, ChatManajemen())
        }
    }
}