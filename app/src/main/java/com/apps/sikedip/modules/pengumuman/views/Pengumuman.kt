package com.apps.sikedip.modules.pengumuman.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.apps.sikedip.modules.pengumuman.presenter.PengumumanPresenter

class Pengumuman : Fragment(){
    lateinit var presenter:PengumumanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pengumuman_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = PengumumanPresenter(this)
    }
}