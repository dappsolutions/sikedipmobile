package com.apps.sikedip.modules.sakit.presenter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.dashboard.model.Sakit
import com.apps.sikedip.modules.dashboard.model.SakitResponses
import com.apps.sikedip.modules.sakit.view.SakitView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.sakit_view.*
import org.jetbrains.anko.doAsync

class SakitPresenter(val main:SakitView){
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataIzinTaruna()
    }

    fun dataIzinTaruna() {
        val url = ApiService.URL.baseUrl(baseUrl,"sakit", "getDataIzinTarunaList")
        val params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna
        params["user"] = Session(main.context).userId

        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, SakitResponses::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<Sakit>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtJam = view.findViewById(R.id.txtJam) as TextView
                val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                val txtKeterangan = view.findViewById(R.id.txtKeterangan) as TextView

                txtNama.text = data[position].nama_taruna
                txtTanggal.text = data[position].tanggal
                txtJam.text = data[position].jam
                txtKeterangan.text = data[position].keterangan
            }
        }, main.activity!!, R.layout.list_taruna_sakit)
        Helper(main.activity, main.rvListSakit, adapter).setRecycleview()
    }
}