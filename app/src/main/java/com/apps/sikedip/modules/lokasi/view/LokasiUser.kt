package com.apps.sikedip.modules.lokasi.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.sikedip.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import com.mapbox.mapboxsdk.Mapbox.getApplicationContext
import android.support.v4.content.ContextCompat




class LokasiUser : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap


    private var gps: GPSTracker? = null
    var lat: Double = 0.0
    var long: Double = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.lokasi_view, container, false)
        setCurrentLokasi()
        var mapFragment = this.activity?.supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance()
            mapFragment.getMapAsync(this)
        }
        val transaction = childFragmentManager.beginTransaction()
        // R.id.map is a layout
        transaction.replace(R.id.map, mapFragment).commit()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
//        val sydney = LatLng(-34.0, 151.0)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

        val currentPosition = LatLng(lat, long)
        mMap.addMarker(MarkerOptions().position(currentPosition).title("Lokasi"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPosition))
    }

    fun setCurrentLokasi() {
        try {
            if (ContextCompat.checkSelfPermission(
                    getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        gps = GPSTracker(context)
        if (gps!!.canGetLocation()) {
            lat = gps!!.getLatitude()
            long = gps!!.getLongitude()
        } else {
            gps!!.showSettingsAlert()
        }
    }
}