package com.apps.sikedip.modules.izin.presenter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.izin.model.IzinModel
import com.apps.sikedip.modules.izin.model.IzinModelResponse
import com.apps.sikedip.modules.izin.view.Izin
import com.google.gson.Gson
import kotlinx.android.synthetic.main.izin_view.*
import org.jetbrains.anko.doAsync

class IzinPresenter (val main:Izin){
    lateinit var adapter:RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataIzinTaruna()
    }

    fun dataIzinTaruna() {
        val url = ApiService.URL.baseUrl(baseUrl,"izin", "getDataIzinTaruna")
        val params = HashMap<String, String>()
        params["taruna"] = Session(main.context).taruna
        params["user"] = Session(main.context).userId

        main.loading.visibility = VISIBLE
        doAsync {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, IzinModelResponse::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<IzinModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtNama = view.findViewById(R.id.txtNama) as TextView
                val txtJenis = view.findViewById(R.id.txtJenis) as TextView
                val txtJam = view.findViewById(R.id.txtJam) as TextView
                val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                val txtSemester = view.findViewById(R.id.txtSemester) as TextView
                val txtKeterangan = view.findViewById(R.id.txtKeterangan) as TextView

                txtNama.text = data[position].nama_taruna
                txtJenis.text = data[position].jenis
                txtTanggal.text = data[position].tanggal
                txtSemester.text = "Semester "+data[position].semester
                txtJam.text = data[position].jam
                txtKeterangan.text = data[position].keterangan
            }
        }, main.activity!!, R.layout.list_izin)
        Helper(main.activity, main.rvListIzin, adapter).setRecycleview()
    }
}