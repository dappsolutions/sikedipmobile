package com.apps.sikedip.modules.chat_admin.model

import com.google.gson.annotations.SerializedName

class ChatAdminModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("message")
    var message:String,
    @SerializedName("from_label")
    var from_label:String,
    @SerializedName("from")
    var from:String,
    @SerializedName("hak_akses")
    var hak_akses:String,
    @SerializedName("tgl_kirim")
    var tgl_kirim:String,
    @SerializedName("to")
    var to:String,
    @SerializedName("createddate")
    var createddate:String
)

data class ChatAdminModelResponse(val data:List<ChatAdminModel>)