package com.apps.sikedip.modules.pengumuman.presenter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.sikedip.R
import com.apps.sikedip.libraries.*
import com.apps.sikedip.modules.pengumuman.model.PengumumanModel
import com.apps.sikedip.modules.pengumuman.model.PengumumanModelResponse
import com.apps.sikedip.modules.pengumuman.views.Pengumuman
import com.google.gson.Gson
import kotlinx.android.synthetic.main.pengumuman_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PengumumanPresenter (val main:Pengumuman){
    lateinit var adapter: RvAdapter
    val baseUrl = Session(main.context).baseUrl
    init {
        dataPengumuman()
    }

    fun dataPengumuman() {
        val url = ApiService.URL.baseUrl(baseUrl,"pengumuman", "getData")
        val params = HashMap<String, String>()

        main.loading.visibility = VISIBLE
        GlobalScope.launch (Dispatchers.Main) {
            Network(main.context).volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    main.loading.visibility = GONE
                    val data = Gson().fromJson(result, PengumumanModelResponse::class.java)
                    setListData(data.data)
                }

                override fun onError(error: VolleyError) {
                    main.loading.visibility = GONE
                    val msg = Error().checkOnErrorVolleyNetworkWithMessage(error)
                    Message(main.context).showMessage(msg)
                }
            })
        }
    }

    fun setListData(data: List<PengumumanModel>) {
        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback{
            override fun onExecuteViewHolder(view: View, position: Int) {
                val txtJudul = view.findViewById(R.id.txtJudul) as TextView
                val txtIsi = view.findViewById(R.id.txtIsi) as TextView

                txtJudul.text = data[position].judul
                txtIsi.text = data[position].isi
            }
        }, main.activity!!, R.layout.list_kesalahan)
        Helper(main.activity, main.rvListPengumuman, adapter).setRecycleview()
    }
}